/*******************************************************************************
 * Ech2o, a spatially-distributed, ecohydrologic simulator
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the 
 *     University of Montana.
 *
 *     Ech2o is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Ech2o is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Ech2o.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta
 *******************************************************************************/
/*
 * CheckMaps.cpp
 *
 *  Created on: Feb 10, 2011
 *      Author: Marco.Maneta
 */

#include "Basin.h"

void Basin::CheckMaps(Control &ctrl) {

	UINT4 r, c;
	UINT4 j = 0;
	UINT4 excep_thrown = 0; //  poor man way  to rethrow string exception outside omp pragma
	UINT4 length = _vSortedGrid.cells.size();
#pragma omp parallel for\
		  default(none) private(r,c,j) shared(length, cout, excep_thrown)
	for (j = 0; j < length; j++) {
		r = _vSortedGrid.cells[j].row;
		c = _vSortedGrid.cells[j].col;
		try {

			if (_vSortedGrid.cells[j].dir < 1
					|| _vSortedGrid.cells[j].dir > 9)
				throw string("The drain direction map is not a valid ldd map...\n");

			if (_slope->matrix[r][c] == _slope->nodata)
				throw string("Slope map contains no data values inside the valid domain...\n");

			if (_slope->matrix[r][c] == 0)
				_slope->matrix[r][c] = MIN_SLOPE;
			if (_slope->matrix[r][c] <= 0)
				throw string("Slope map contains negative or zero values inside the valid domain...\n");

			if (_Ksat->matrix[r][c] == _Ksat->nodata)
				throw string("Ksat map contains no data values inside the valid domain...\n");
			if(_fImperv->matrix[r][c] < 0)
		        throw string("Fraction of impervious area is negative inside the valid domain...\n");
			if(_fImperv->matrix[r][c] > 1)
				throw string("Fraction of impervious area is larger than 1 inside the valid domain...\n");
			if (_slope->matrix[r][c] <= 0)
				throw string("Slope map contains negative or zero values inside the valid domain...\n");

			if (_porosity->matrix[r][c] == _porosity->nodata)
				throw string("Porosity map contains no data values inside the valid domain...\n");

			if (_porosity->matrix[r][c] <= 0)
				throw string("Porosity map contains negative or zero values inside the valid domain...\n");

			if (_psi_ae->matrix[r][c] == _psi_ae->nodata)
				throw string("Air entry pressure map contains no data values inside the valid domain...\n");

			if (_psi_ae->matrix[r][c] <= 0)
				throw string("Air entry pressure map contains negative or zero values inside the valid domain...\n");


			if (_BClambda->matrix[r][c] == _BClambda->nodata)
				throw string("Brooks and Cory lambda map contains no data values inside the valid domain...\n");

			if (_BClambda->matrix[r][c] <= 2) {
				_BClambda->matrix[r][c] = 2;
				throw string("WARNING: Brooks and Corey lambda map is too small, switching to minimum value of 2...\n");
			}

			if (_theta_r->matrix[r][c] == _theta_r->nodata)
				throw string("residual moisture map contains no data values inside the valid domain...\n");

			if (_theta_r->matrix[r][c] <= 0)
				throw string("residual moisture map contains negative or zero values inside the valid domain...\n");

			if (_theta_r->matrix[r][c] > _porosity->matrix[r][c])
				throw string("Residual soil moisture map is larger porosity than inside the valid domain...\n");

			if (_soildepth->matrix[r][c] == _soildepth->nodata)
				throw string("soil depth map contains no data values inside the valid domain...\n");

			if (_soildepth->matrix[r][c] < 0)
				throw string("soil depth map contains negative values inside the valid domain...\n");

			if (_paramWc->matrix[r][c] == _paramWc->nodata)
				throw string("Soil parameter Wc map contains no data values inside the valid domain...\n");

			if (_paramWc->matrix[r][c] <= 0)
				throw string("Soil parameter Wc map contains negative or zero values inside the valid domain...\n");

			if (_paramWp->matrix[r][c] == _paramWp->nodata)
				throw string("Soil parameter Wp map contains no data values inside the valid domain...\n");

			if (_paramWp->matrix[r][c] <= 0)
				throw string("Soil parameter Wp map contains negative or zero values inside the valid domain...\n");

			if (_meltCoeff->matrix[r][c] == _meltCoeff->nodata)
				throw string("Snowmelt coefficient map contains no data values inside the valid domain...\n");

			if (_meltCoeff->matrix[r][c] <= 0)
				throw string("Snowmelt coefficient map contains negative or zero values inside the valid domain...\n");

			if (_snow->matrix[r][c] == _snow->nodata)
				throw string("Initial SWE map contains no data values inside the valid domain...\n");

			if (_snow->matrix[r][c] < 0)
				throw string("Initial SWE map contains negative values inside the valid domain...\n");

			if (_albedo->matrix[r][c] == _albedo->nodata)
				throw string("Albedo map contains no data values inside the valid domain...\n");

			if (_albedo->matrix[r][c] <= 0)
				throw string("Albedo map contains negative or zero values inside the valid domain...\n");

			if (_emiss_surf->matrix[r][c] == _emiss_surf->nodata)
				throw string("Surface emissivity map contains no data values inside the valid domain...\n");

			if (_emiss_surf->matrix[r][c] <= 0)
				throw string("Surface emissivity map contains negative or zero values inside the valid domain...\n");

			if (_soil_dry_heatcap->matrix[r][c] == _soil_dry_heatcap->nodata)
				throw string("Soil dry heat capacity map contains no data values inside the valid domain...\n");

			if (_soil_dry_heatcap->matrix[r][c] <= 0)
				throw string("Soil dry heat capacity map contains negative or zero values inside the valid domain...\n");

			if (_soil_dry_thermcond->matrix[r][c] == _soil_dry_thermcond->nodata)
				throw string("Dry soil thermal conductivity map contains no data values inside the valid domain...\n");

			if (_soil_dry_thermcond->matrix[r][c] <= 0)
				throw string("Dry soil thermal conductivity map contains negative or zero values inside the valid domain...\n");

			if (_dampdepth->matrix[r][c] == _dampdepth->nodata)
				throw string("Thermal soil damping depth map contains no data values inside the valid domain...\n");

			if (_dampdepth->matrix[r][c] <= 0)
				throw string("Thermal soil damping depth map contains negative or zero values inside the valid domain...\n");

			if (_Temp_d->matrix[r][c] == _Temp_d->nodata)
				throw string("Soil temp at damp depth map contains no data values inside the valid domain...\n");

			if (_soilmoist1->matrix[r][c] == _soilmoist1->nodata)
				throw string("Initial soil moisture map contains no data values inside the valid domain...\n");

			if (_soilmoist1->matrix[r][c] <= 0)
				throw string("Topsoil Initial soil moisture map contains negative or zero values inside the valid domain...\n");

			if (_soilmoist1->matrix[r][c] <= _theta_r->matrix[r][c])
				throw string("Topsoil Initial soil moisture map is lower or equal than residual moisture inside the valid domain...\n");

			if (_soilmoist2->matrix[r][c] <= _theta_r->matrix[r][c])
				throw string("Initial soil moisture map in soil layer 2 is lower or equal than residual moisture inside the valid domain...\n");

			if (_soilmoist3->matrix[r][c] <= _theta_r->matrix[r][c])
				throw string("Initial soil moisture map in soil layer 3  is lower or equal than residual moisture inside the valid domain...\n");

			if (_soilmoist1->matrix[r][c] > _porosity->matrix[r][c])
				throw string("Topsoil Initial soil moisture map is larger than porosity inside the valid domain...\n");

			if (_soilmoist2->matrix[r][c] > _porosity->matrix[r][c])
				throw string("Initial soil moisture map in soil layer 2 is larger than porosity inside the valid domain...\n");

			if (_soilmoist3->matrix[r][c] > _porosity->matrix[r][c])
				throw string("Initial soil moisture map in soil layer 2  is larger than porosity inside the valid domain...\n");

			if (_channelwidth->matrix[r][c] < 0)
				throw string("The channel width map contains negative values\n");

			if (_soilmoist2->matrix[r][c] == _soilmoist2->nodata)
				throw string("Initial soil moisture map in layer 2 contains no data values inside the valid domain...\n");

			if (_soilmoist3->matrix[r][c] == _soilmoist3->nodata)
				throw string("Initial soil moisture map in layer 3 contains no data values inside the valid domain...\n");

			if (_soilmoist2->matrix[r][c] < _theta_r->matrix[r][c])
				throw string("Initial soil moisture map in layer 2 is lower than residual moisture inside the valid domain...\n");

			if (_soilmoist2->matrix[r][c] > _porosity->matrix[r][c])
				throw string("Initial soil moisture map in layer 2 is larger than porosity inside the valid domain...\n");

			if (_soilmoist3->matrix[r][c] < _theta_r->matrix[r][c])
				throw string("Initial soil moisture map in layer 3 is lower than residual moisture inside the valid domain...\n");

			if (_soilmoist3->matrix[r][c] > _porosity->matrix[r][c])
				throw string("Initial soil moisture map in layer 3 is larger than porosity inside the valid domain...\n");

		} catch (string &e) {
			std::cout << e << std::endl;
			std::cout << "In row " << r << " col " << c << std::endl;
            #pragma omp atomic
			++excep_thrown;
		}

	}//for

	if (excep_thrown != 0)
		throw;
}
